@extends('master')
@section('content')
    <div class="card" style="margin-top:140px; width: 100rem; height:30rem">
        <div class="card-body font-weight-bold text-center">
            <h1>Tim Kami</h1>
        </div>

        <div class="container " style="margin-bottom: 250px; margin-left:100px">
            <div class="row">
    <div class="col-sm">
        <img class="ml-lg-4" src="{{ asset('img/bg-img/profile-tim.jpg') }}" width="30%">
        <h6 class="">Yudisthira Iriana Putra</h6>
        <p class="ml-lg-4">"Web Developer"</p>
    </div>
    <div class="col-sm">
        <img class="ml-lg-3" src="{{ asset('img/bg-img/profile-tim.jpg') }}" width="30%">
        <h6 class="ml-md-5">Irwan</h6>
        <p class="ml-lg-3">"Web Developer"</p>
    </div>
    <div class="col-sm">
        <img class="ml-lg-4" src="{{ asset('img/bg-img/profile-tim.jpg') }}" width="30%">
        <h6 class="ml-md-3">	Fariza Aulia Putri</h6>
        <p class="ml-md-4">"Web Developer"</p>
    </div>

</div>
        </div>
    </div>
@endsection
