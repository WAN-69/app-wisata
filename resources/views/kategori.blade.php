@extends('master')
@section('content')
<div class="container-fluid" style="padding-top: 200px">
    <div class="alert alert-warning " style="100px">Silahkan Login untuk
        menambahkan
        kategori baru</div>
    <div class="card ">
        <div class="card-header">
            <h4>Daftar Kategori</h4>
        </div>
        <div class="card-body ">
            <table class="table table-hover " id="myTable">
                <thead class="thead-light text-md-center ">
                    <tr>
                        <th scope="col ">#</th>
                        <th scope="col ">Jenis Wisata</th>
                        <th scope="col ">Deskripsi</th>
                    </tr>
                </thead>
                <tbody class="text-md-center">
                    @forelse ($kategori as $key=>$value)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$value->kategori}}</td>
                        <td>{{$value->deskripsi}}</td>

                    </tr>
                    @empty
                    <tr colspan="5">
                        <td class="">*Belum ada data*</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('yajra-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
@endpush

@push('yajra-js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@endpush
