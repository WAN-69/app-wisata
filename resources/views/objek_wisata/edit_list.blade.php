@extends('master')

@section('content')
<!DOCTYPE html>
    <html>
    <head>
    <style>


    
    div.judul {
    margin-top: 50px;
    text-align: center;
    }
    body {
    color: white;
    }




    </style>
    </head>

    <body>
    <div class="row h-50 align-items-center justify-content-center">
        <div class="judul">
            <divclass="hero-content">
                 <h2 style="color: white">Form Edit {{$data2->id}}</h2>
            </div>
        </div>

    </div>

    

    <div class='form'>
        <form action='/review/list/{{$data2->id}}' method='POST' enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                    <label for="nama">Nama Objek Wisata</label>
                    <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama Objek Wisata" value="{{old('nama', $data2-> nama )}}">
                </div>
                <div class="form-group">
                    <label for="review">Review</label>
                    <textarea class="form-control" id="review" name='review' rows="10" value="{{old('review', $data2-> review )}}"></textarea>

                </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
    </body>

@endsection
