@extends('master')

@section('content')
<!DOCTYPE html>
    <html>
    <head>
    <style>


    
    div.judul {
    margin-top: 100px;
    text-align: center;
    }
    body {
    color: white;
    }




    </style>
    </head>

    <body>
    <div class="row h-50 align-items-center justify-content-center ">
        <div class="judul">
            <div class='hero-content'>
                <h2>Form Edit {{$data2->id}}</h2>
            </div>
        </div>

    </div>
    <div class="container h-50">

        <div class="row h-50 align-items-center justify-content-center mt-5">

            <div class='form'>
                <form action='/objek/moderasi/{{$data2->id}}' method='POST' enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nama">Nama Objek Wisata</label>
                        <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama Objek Wisata" value="{{old('nama', $data2-> nama )}}">
                    </div>
                    <div class="form-group">
                        <label for="kategori">Kategori</label>
                        <select class="form-control" id="kategori" name='kategori'>
                        <option>Kuliner</option>
                        <option>Wisata Alam</option>
                        <option>Wisata Sejarah</option>
                        <option>Hotel</option>
                        <option>Shopping</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" name='deskripsi' rows="5" value="{{old('deskripsi', $data2-> deskripsi )}}"></textarea>

                    </div>
                    <div class="form-group">
                        <label for="gambar">Gambar</label>
                        <br>
                        <input id="gambar" name="gambar" type="file" class="file" data-browse-on-zone-click="true">
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </body>

@endsection
<!--<!DOCTYPE html>
    <html>
    <head>
    <style>


    
    div.judul {
    margin-top: 100px;
    text-align: center;
    }
    body {
    color: white;
    }




    </style>
    </head>

    <body>
    <div class="row h-100 align-items-center justify-content-center ">
        <div class="judul">
            <h2>Form Edit {{$data2->id}}</h2>
        </div>

    </div>

    <div class='form'>
        <form action='/objek/moderasi' method='POST' enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama Objek Wisata</label>
                <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama Objek Wisata" value="{{old('nama', $data2-> nama )}}>
            </div>
            <div class="form-group">
                <label for="kategori">Kategori</label>
                <select class="form-control" id="kategori" name='kategori'>
                <option>Kuliner</option>
                <option>Wisata Alam</option>
                <option>Wisata Sejarah</option>
                <option>Hotel</option>
                <option>Shopping</option>
                </select>
            </div>
            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea class="form-control" id="deskripsi" name='deskripsi' rows="5" value="{{old('deskripsi', $data2-> deskripsi )}}></textarea>

            </div>
            <div class="form-group">
                <label for="gambar">Gambar</label>
                <br>
                <input id="gambar" name="gambar" type="file" class="file" data-browse-on-zone-click="true">
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
    </body>-->
