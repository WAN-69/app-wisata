@extends('master')

@section('content')
@push('objek-css')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<style>
    div.gallery {
        border: 1px solid #ccc;
    }

    div.gallery:hover {
        border: 1px solid #777;
    }

    div.gallery img {
        width: 100%;
        height: auto;
    }

    div.desc {
        padding: 15px;
        text-align: center;
    }

    * {
        box-sizing: border-box;
    }

    .responsive {
        padding: 0 6px;
        float: left;
        width: 24.99999%;
    }

    @media only screen and (max-width: 700px) {
        .responsive {
            width: 49.99999%;
            margin: 6px 0;
        }
    }

    @media only screen and (max-width: 500px) {
        .responsive {
            width: 100%;
        }
    }

    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    body {
        color: white;
    }

    p {
        color: white;
    }

    div.form_baru {
        text-align: left;
    }
</style>
@endpush

</head>

<body>

    <div class="container h-50">
        <div class="row h-100 align-items-center justify-content-center mt-5">
            <div class="hero-content">
                <h2>Daftar Objek Wisata</h2>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row h-50 align-items-center justify-content-center">

            <div class="responsive">
                <div class="gallery">

                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> <a href="/objek/info/kasepuhan">Keraton Kasepuhan</a></strong></div>
                    <a target="_blank" href="{{asset('/img/objek/kasepuhan.jpg')}}">
                        <img src="{{asset('/img/objek/kasepuhan.jpg')}}" alt="Keraton Kasepuhan"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Keraton Kasepuhan Cirebon, Keraton tertua di Cirebon Peninggalan Sunan Gunung Jati</div>-->

            </div>


            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Museum Linggarjati </strong></div>
                    <a target="_blank" href="{{asset('/img/objek/linggarjati.jpg')}}">
                        <img src="{{asset('/img/objek/linggarjati.jpg')}}" alt="Museum LInggarjati"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Gedung Peundingan Linggarjati Kuningan</div>-->

            </div>

            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Pantai Tirtamaya </strong></div>
                    <a target="_blank" href="{{asset('/img/objek/tirtamaya.jpg')}}">
                        <img src="{{asset('/img/objek/tirtamaya.jpg')}}" alt="Pantai Tirtamaya"
                            style="width:600px; height:200px;;">
                    </a>
                </div>
                <!--<div class="desc">Wisata Alam Pantai Tirtamaya Indramayu</div>-->

            </div>

            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Bukit Panyaweuyan </strong></div>
                    <a target="_blank" href="{{asset('/img/objek/bukit2.jpg')}}">
                        <img src="{{asset('/img/objek/bukit2.jpg')}}" alt="Bukit Panyaweuyan"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata Alam Pantai Tirtamaya Indramayu</div>-->

            </div>

        </div>

        <div class="row h-50 align-items-center justify-content-center mt-5">
            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Nasi Jamblang Mang Dul </strong></div>
                    <a target="_blank" href="{{asset('/objek/jamblang.jpg')}}">
                        <img src="{{asset('/img/objek/jamblang.jpg')}}" alt="Nasi Jamblang"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Gedung Peundingan Linggarjati Kuningan</div>-->
            </div>

            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Empal Gentong Amarta </strong></div>
                    <a target="_blank" href="{{asset('/objek/amarta.jpg')}}">
                        <img src="{{asset('/img/objek/amarta.jpg')}}" alt="Empal Gentong"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Gedung Peundingan Linggarjati Kuningan</div>-->
            </div>

            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> CSB Mall </strong></div>
                    <a target="_blank" href="{{asset('/image/objek/csb.jpg')}}">
                        <img src="{{asset('/img/objek/csb.jpg')}}" alt="CSB" style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Gedung Peundingan Linggarjati Kuningan</div>-->
            </div>

            <div class="responsive">
                <div class="gallery">
                    <div class="nama"
                        style="background-color:white; color:black; text-align:center; font-family:'Arial Black'">
                        <strong> Grage Mall </strong></div>
                    <a target="_blank" href="{{asset('/img/objek/grage.jpg')}}">
                        <img src="{{asset('/img/objek/grage.jpg')}}" alt="Grage Mall"
                            style="width:600px; height:200px;">
                    </a>
                </div>
                <!--<div class="desc">Wisata sejarah Gedung Peundingan Linggarjati Kuningan</div>-->
            </div>


        </div>

    </div>


    <div class="container h-50 mt-5">

        <div class='form_baru'>
            <p><strong> Memiliki rekomendasi tempat wisata baru? </strong></p>
            <p><strong>Silahkan berkontribusi melalui laman berikut </strong></p>
            <a href="objek/tambah" class="btn btn-info btn-sm">Objek Baru</a>

        </div>
    </div>
</body>

</html>


@endsection

<!--<div style="padding:6px;">
    <p>Memiliki rekomendasi tempat wisata baru? </p>
    <p>Silahkan berkontribusi melalui laman berikut</p>
    <a href="objek/tambah" class="btn btn-info btn-sm">Objek Baru</a>
</div>

<!DOCTYPE html>
    <html>
    <head>
    <style>
    body {
    color: white;
    }
    p {
    color: white;
    }


    div.gallery {
    border: 1px solid #ccc;
    }

    div.gallery:hover {
    border: 1px solid #777;
    }

    div.gallery img {
    width: 100%;
    height: auto;
    }

    div.desc {
    padding: 15px;
    text-align: center;
    }

    * {
    box-sizing: border-box;
    }

    .responsive {
    padding: 0 6px;
    float: left;
    width: 24.99999%;
    }

    @media only screen and (max-width: 700px) {
    .responsive {
        width: 49.99999%;
        margin: 6px 0;
    }
    }

    @media only screen and (max-width: 500px) {
    .responsive {
        width: 100%;
    }
    }

    .clearfix:after {
    content: "";
    display: table;
    clear: both;
    }

    div.judul {
    margin-top: 20px;
    color: white;
    }-->
