@extends('master')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
 
<div class="container h-50">

        
        <div class='hero-content'>
            <h3 style="color: white">Informasi Wisata</h3>
        </div>

        <div class="card" style="width:400px">
            <img class="card-img-top" src="{{asset('/img/objek/kasepuhan.jpg')}}" alt="Card image" style="width:100%">
            <div class="card-body">
                <h4 class="card-title">Keraton Kasepuhan</h4>
                <p class="card-text">Keraton tertua di Cirebon</p>
            </div>
        </div>
        <a href="/review" class="btn btn-primary">Review</a>

    
</div>
</body>
</html>

@endsection