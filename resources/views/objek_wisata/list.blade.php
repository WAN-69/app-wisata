@extends('master')

@section('content')

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Moderasi Objek Wisata</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
    div.mod {
    margin-top: 100px;
    padding: 25px;
    }

  </style>
</head>
<body>

<div class="container">
    <div class='mod'>
        <div class='hero-content'>
            <h2>Moderasi Review</h2>
        </div>
        <table class="table table-dark">
        <thead>
        <tr>
            <th>No.</th>
            <th>Nama Objek</th>
            <th>Review</th>
        </tr>
        </thead>
        <tbody>
            @foreach($data as $key => $data)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->review}}</td>

                <td> 
                    <a href="/review/{{$data->id}}" class="btn btn-info btn-sm">info</a>
                    <a href="/review/{{$data->id}}/edit" class="btn btn-success btn-sm">edit</a>
                    <form action="/review/{{$data->id}}" method="post" style = display:inline-block;><br>
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>

            </tr>
            @endforeach
        </tbody>

    </div>
  </table>
</div>

</body>
</html>

@endsection
