@extends('master')

@section('content')
<style>
    div.mod {
    margin-top: 100px;
    padding: 25px;
    text-align: center;
    }
    body{
        color: white;
    }

  </style>

<div class="mod">
    <div class='hero-content'>
        <h2>Data Calon Objek Wisata</h2>
    </div>
    <br>
    <ul>
        <li><h3 style="color:white; font-family:verdana;">{{$data1->nama}}</h3></li>
        <li><img src="{{asset('/img/'.$data1->gambar)}}" alt="gambar" width=50%></li>
        <li><h5 style="color: white; font-family:courier;">{{$data1->deskripsi}}</h5></li>
    </ul>

</div>
@endsection