@extends('master')

@section('content')
<!DOCTYPE html>
    <html>
    <head>
    <style>


    
    div.judul {
    margin-top: 100px;
    text-align: center;
    }
    body {
    color: white;
    }




    </style>
    </head>

    <body>
    <div class="container h-50">
        <div class="row h-100 align-items-center justify-content-center mt-5">
                <div class="hero-content">
                    <h2>Review</h2>
                </div>
        </div>
    </div>


    <div class='form'>
        <form action='/review/list' method='POST' >
            @csrf
            <div class="col-md-12">
                <div class="form-group">
                    <label for="nama">Nama Objek Wisata</label>
                    <input type="text" class="form-control" id="nama" name='nama' placeholder="Nama Objek Wisata">
                </div>
                <div class="form-group">
                    <label for="review">Review</label>
                    <textarea class="form-control" id="review" name='review' rows="10"></textarea>

                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        </form>
    </div>
    </body>

@endsection
