@extends('master')
@section('content')
<div class="container " style="padding-top: 200px">
    <div class="card">
        <div class="card-header">
            <h2> Update Kategori {{$kategori->kategori}} </h2>
        </div>
        <div class="card-body">
            <form action="/kategori/{{$kategori->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="kategori">Jenis Kategori</label>
                    <input type="text" class="form-control" name="kategori" value="{{$kategori->kategori}}" id="kategori" placeholder="Masukkan kategori">
                    @error('kategori')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi Kategori</label>
                    <input type="text" class="form-control" name="deskripsi" value="{{$kategori->deskripsi}}" id="deskripsi" placeholder="Masukkan deskripsi">
                    @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
