@extends('master')
@section('content')
<div class="container style=" style="padding-top: 200px; padding-left: 400px;">
    <div class="card" style="width: 18rem;">
        <img src="{{ asset('img/bg-img/preview-kategori.png') }}" class="card-img-top">
        <div class="card-img-overlay" style="margin: 90px">Nah... Disini Fotonya</div>
        <div class="card-body">
            <h5 class="card-title">Di sini Judul Kamu</h5>
            <p class="card-text">Di sini Deskripsi dari judul kamu .</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Kategori : {{$kategori->kategori}}</li>
        </ul>
    </div>
</div>
@endsection
