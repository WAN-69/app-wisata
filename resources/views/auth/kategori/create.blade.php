@extends('master')
@section('content')
<div class="container" style="padding-top: 200px">
    <div class="card">
        <h5 class="card-header">Tambah Kategori Baru</h5>
        <div class="card-body">
            <form class="needs-validation" action="{{ route('simpan.kategori') }}" method="POST">
                @csrf
                <div class="form-group" novalidate>
                    <label for="kategori">Jenis Wisata</label>
                    <input type="text" class="form-control" name="kategori" id="kategori"
                        placeholder="Masukkan kategori" required>
                    <div class="invalid-tooltip">
                        Mohon Diisi
                    </div>
                    @error('kategori')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" id="deskripsi"
                        placeholder="Masukkan Deskripsi (Optional)">
                    @error('deskripsi')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
    </div>
</div>
@endsection
