@extends('master')
@section('content')
<div class="container-fluid" style="padding-top: 200px">
    <div class="card ">
        <div class="card-header">
            <a href="{{ route('bikin.kategori') }}" class="btn btn-primary ">Tambah</a>
        </div>
        <div class="card-body ">
            <table class="table table-hover " id="myTable">
                <thead class="thead-light text-md-center ">
                    <tr>
                        <th scope="col ">#</th>
                        <th scope="col ">Jenis Wisata</th>
                        <th scope="col ">Deskripsi</th>
                        <th scope="col ">Aksi</th>
                    </tr>
                </thead>
                <tbody class="text-md-center">
                    @forelse ($kategori as $key=>$value)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$value->kategori}}</td>
                        <td>{{$value->deskripsi}}</td>
                        <td>
                            <form action="/kategori/{{ $value->id }}" method="POST">
                                <a href="/kategori/{{ $value->id }}" class="btn btn-info "> Preview</a>
                                <a href="/kategori/{{ $value->id }}/edit" class="btn btn-primary ">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger text-white" value="Delete">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr colspan="5">
                        <td class="">*Belum ada data*</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('yajra-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
@endpush

@push('yajra-js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@endpush
