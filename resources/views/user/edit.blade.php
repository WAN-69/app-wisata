@extends('master')
@section('content')
<div class="container " style="padding-top: 200px">
    <div class="card">
        <div class="card-header">
            <h2> Update User {{$user->name}} </h2>
        </div>
        <div class="card-body">
            <form action="/user/{{$user->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Nama name</label>
                    <input type="text" class="form-control" name="name" value="{{$user->name}}" id="name"
                        placeholder="Masukkan name">
                    @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" value="{{$user->email}}" id="email"
                        placeholder="Masukkan email">
                    @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" name="password" value="{{$user->password}}" id="deskripsi"
                        placeholder="Masukkan password">
                    @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" value="{{$user->deskripsi}}" id="deskripsi"
                        placeholder="Masukkan deskripsi">
                    @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
</div>
@endsection
