@extends('master')
@section('content')
<div class="container-fluid" style="padding-top: 200px">
    <div class="card ">
        <div class="card-header">
            <a href="{{ route('bikin.user') }}" class="btn btn-primary ">Tambah</a>
        </div>
        <div class="card-body ">
            <table class="table table-hover " id="myTable">
                <thead class="thead-light text-md-center ">
                    <tr>
                        <th scope="col ">#</th>
                        <th scope="col ">Nama</th>
                        <th scope="col ">Email</th>
                        <th scope="col ">Aksi</th>
                    </tr>
                </thead>
                <tbody class="text-md-center">
                    @forelse ($user as $key=>$value)
                    <tr>
                        <th scope="row">{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>
                            <form action="/user/{{ $value->id }}" method="POST">
                                <a href="/user/{{ $value->id }}" class="btn btn-info "> Show</a>
                                <a href="/user/{{ $value->id }}/edit" class="btn btn-primary ">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger text-white" value="Hapus">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr colspan="5">
                        <td class="">*Belum ada data*</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('yajra-css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
@endpush

@push('yajra-js')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
    });
</script>
@endpush
