@extends('master')
@section('content')
<div class="container style=" style="padding-top: 200px; padding-left:400px">
    <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{ asset('assets/img/avatar/avatar-1.png') }}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">{{ $user->name }}</h5>
            <p class="card-text">{{$user->deskripsi}}</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">{{ $user->email }}</li>
        </ul>
    </div>
</div>
@endsection
