@extends('master')
@section('content')
<div class="container" style="padding-top: 200px">
    <div class="card">
        <h5 class="card-header">Tambah user Baru</h5>
        <div class="card-body">
            <form class="needs-validation" action="{{ route('simpan.user') }}" method="POST">
                @csrf
                <div class="form-group" novalidate>
                    <label for="name">Nama</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Kamu"
                        required>
                    <div class="invalid-tooltip">
                        Mohon Diisi
                    </div>
                    @error('name')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" name="email" id="deskripsi" placeholder="Masukkan email">
                    @error('email')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="text" class="form-control" name="password" id="password"
                        placeholder="Masukkan password">
                    @error('password')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" name="deskripsi" id="deskripsi"
                        placeholder="Deskripsikan tentang dirimu (Optional)">
                    @error('deskripsi')
                    <div class="badge badge-danger mt-2">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>
        </div>
    </div>
</div>
@endsection
