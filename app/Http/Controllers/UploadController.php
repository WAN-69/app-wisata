<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UploadController extends Controller
{
    public function simpan(Request $request){
       // dd($request->all());
       $nama = $request -> gambar;
       $namaFile = $nama -> getClientOriginalName();

       $upload = DB::table('objek_wisata')->insert([
        'nama' => $request['nama'],
        'deskripsi' => $request['deskripsi'],
        'gambar' => $namaFile,
       ]);

       $nama -> move (public_path().'/img', $namaFile);

       return redirect('/objek/moderasi');

    }

    public function info($id){
        $data1 = DB::table('objek_wisata')->where('id',$id)->first();
        //dd($data1);
        return view('/objek_wisata/info', compact('data1'));
    }

    public function edit($id){
        $data2 = DB::table('objek_wisata')->where('id',$id)->first();
        //dd($data);
        return view('/objek_wisata/edit', compact('data2'));
    }

    public function update($id, Request $request){
        $nama = $request -> gambar;
        $namaFile = $nama -> getClientOriginalName();

        $update = DB::table('objek_wisata')

              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'deskripsi' => $request['deskripsi'],
                  'gambar' => $namaFile
                ]);
                $nama -> move (public_path().'/img', $namaFile);

                return redirect('/objek/moderasi');

    }

    public function hapus($id){
        $hapus = DB::table('objek_wisata')
            ->where('id', $id)
            ->delete();
            return redirect('/objek/moderasi');
    }

    public function save(Request $request){
        //dd($request->all());
 
        $upload = DB::table('komen')->insert([
            'nama' => $request['nama'],
            'review' => $request['review'],
        ]);

        return redirect('/review/list');
    }

    public function list_info($id){
        $data1 = DB::table('komen')->where('id',$id)->first();
        //dd($data1);
        return view('/objek_wisata/list_info', compact('data1'));
    }

    public function list_edit($id){
        $data2 = DB::table('komen')->where('id',$id)->first();
        return view('/objek_wisata/edit_list', compact('data2'));
    }

    public function ubah($id, Request $request){
        $update = DB::table('komen')

              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'review' => $request['review'],
                ]);

                return redirect('/review/list');

    }

    public function hilang($id){
        $hapus = DB::table('komen')
            ->where('id', $id)
            ->delete();
            return redirect('/review/list');
    }





 

}
