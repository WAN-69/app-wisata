<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ObjekController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //$data = DB::table('objek_wisata')->get();
        return view('/objek_wisata/objek');
    }

    public function tambah(){
        return view('auth.objek.tambah');
    }

    public function moderasi(){
        $data = DB::table('objek_wisata')->get();
        return view('/objek_wisata/moderasi', compact('data'));
    }

    public function nama(){
        return view('/objek_wisata/kasepuhan');
    }

    public function review(){
        return view('/objek_wisata/review');
    }

    public function list(){
        $data = DB::table('komen')->get();
        //dd($data->all());

        return view('/objek_wisata/list', compact('data'));

    }







};
