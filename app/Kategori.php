<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\KategoriController;

class Kategori extends Model
{
    protected $guarded = [];
    protected $table = "kategori";
}
