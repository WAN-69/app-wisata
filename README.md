# Kelompok 18
# Anggota Kelompok


* Yudisthira Iriana Putra => membuat ERD, master template, CRUD Objek wisata dan CRUD Review
* Irwan => membuat CRUD kategori, authentication, eloquent ORM, memasang berbagai package, dan deploy hosting
* Fariza Aulia Putri => CRUD profile

# Tema Project
Aplikasi Wisata

# ERD

![contoh_final_project](/uploads/55590df266aa556bc9326b0fb4dbde54/contoh_final_project.png)

# Link Video

Link Video Demo Aplikasi: https://drive.google.com/drive/folders/1TU5d_hdW-w5NzGAQoRaG09Px-e_tW0Eg?usp=sharing

Link Deploy: http://webku-dong.my.id/
