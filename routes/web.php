<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('kategori', 'KategoriController')
    ->names([
        'index' => 'home.kategori',
        'create' => 'bikin.kategori',
        'show' => 'tampil.kategori',
        'store' => 'simpan.kategori',
        'edit' => 'edit.kategori',
        'update' => 'update.kategori',
        'destroy' => 'hapus.kategori'
    ]);

Route::resource('user', 'UserController')
    ->names([
        'index' => 'home.user',
        'create' => 'bikin.user',
        'show' => 'tampil.user',
        'store' => 'simpan.user',
        'edit' => 'edit.user',
        'update' => 'update.user',
        'destroy' => 'hapus.user'
    ]);

Route::get('/login', function () {
    return view ('login.login');
})->name('login');

Route::get('/tim', function () {
    return view ('tim');
})->name('tim');

Route::get('/objek', 'ObjekController@index');

Route::get('/objek/tambah', 'ObjekController@tambah');

Route::get('/objek/moderasi', 'ObjekController@moderasi');

Route::post('/objek/moderasi', 'UploadController@simpan');

Route::get('/objek/{id}', 'UploadController@info');

Route::get('/objek/{id}/edit', 'UploadController@edit');

Route::put('/objek/moderasi/{id}', 'UploadController@update');

Route::delete('/objek/{id}', 'UploadController@hapus');

Route::get('/objek/info/kasepuhan', 'ObjekController@nama');

Route::get('/review', 'ObjekController@review');

Route::get('/review/list', 'ObjekController@list');

Route::post('/review/list', 'UploadController@save');

Route::get('/review/{id}', 'UploadController@list_info');

Route::get('/review/{id}/edit', 'UploadController@list_edit');

Route::put('/review/list/{id}', 'UploadController@ubah');

Route::delete('/review/{id}', 'UploadController@hilang');


Route::get('/objek/komen', 'ObjekController@komen')->name('komen');

Route::get('/objek/tes', 'ObjekController@tes')->name('test');




Auth::routes();

Route::get('/categories', 'HomeController@index')->name('categories');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/objects', 'HomeController@indexobject')->name('objects');
